import { config } from 'dotenv'
import Express from 'express'
import AccessTokenVerifier from './Middlewares/AccessTokenVerifier.js'
import db from './mongoConnection.js'
import AuthRoute from './Routes/AuthRoute.js'
import UserRoute from './Routes/UserRoute.js'
import AdminRoute from './Routes/AdminRoute.js'
import cors from 'cors'
config()
const app = Express()
app.use(Express.json())
app.use(cors())
app.use('/auth', AuthRoute)
app.use("/user", UserRoute)
app.use("/admin", AdminRoute)


app.listen(5000, () => {
  console.log('listening on port 5000')
})
