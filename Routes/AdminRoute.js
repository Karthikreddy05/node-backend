import express from "express";
import mongoConnection from "../mongoConnection.js";
const Router = express.Router();

const Arraytypes = ['skills' , 'sports', 'interests']

Router.post("/search", async (req, res) => {
    console.log(req.body)
    const { searchvalue, type} = req.body;
    try{
    let _dbCon = await mongoConnection.getDbCon();
    let collection = _dbCon.collection("userDetails");
    if(Arraytypes.includes(type)){
        const searchRes = await collection.find({[type]:{$in:[searchvalue]}},{
            projection:{
                _id : 0,
                userID:1,
                firstName :1,
                lastName:1,
                email   :1,
                phonenumber : 1,
            }
        }).toArray();
        console.log(searchRes)
        res.status(201).json({
            success: "searched successfully",
            data: searchRes
        })
    }
    else{
        res.status(201).json({
            success: "searched successfully",
            data: searchRes
        })
    }}catch(error){
        res.status(401).json({
            error: "database error",
        })
    }
});



export default Router;